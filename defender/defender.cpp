#include "defender.h"
DefenseCat::DefenseCat() :
    debugSerial(USBTX, USBRX, 115200),
    omni(),
    imu(JY901_TX, JY901_RX),
    cam(PIXY_TX, PIXY_RX),
    FW(XBEE_TX, XBEE_RX),
    xPid(BALL_KP, BALL_KI, BALL_KD, T_SAMPLE)
{
    ui.lChika(3);
    ui.buzzer = 0.5;
    ballPush.reset();

    imu.confirm();
    imu.resetOffset();
    imu.confirm();

    xPid.setInputLimits(-80, 80);
    xPid.setOutputLimits(-1.1, 1.1);
    xPid.setSetPoint(0);
    xPid.setBias(0);
    xPid.setMode(AUTO_MODE);

    ballPush.reset();
    ballarea.reset();
    behindTimer.reset();
    stopY = false;
    behindFlag = false;
    ballgo = false;
    balldrive = false;
    goback = false;
    // sonicRangeChange = false;
    // xLeft = false;
    // xRight = false;
    lineSonic[BACK] = lineSonic[LEFT] = lineSonic[RIGHT] = false;
    ballStopFlag = true;
    // movedLeft = false;
    // movedRight = false;

    testflag = false;

    ui.buzzer = 0.0;
    threadxPid.start(callback(this, &DefenseCat::xPidLoop));
    threadDribbler.start(callback(this, &DefenseCat::dribblerLoop));
    t.start();
}
void DefenseCat::updateSensors()
{

    if(ui.isPushed(0)) {
        ui.buzzer = 0.5;
        imu.resetOffset();
        Thread::wait(50);
        ui.buzzer = 0.0;
    }
    if(cam.ball.isViewed > 0) {
        findBall = true;
    } else {
        findBall = false;
    }
    dist[0] = sonic.getDistance(BACK);
    if(dist[0] < 60) dist[0] = 60;
    if(dist[0] > 900)  dist[0] = 900;
    if(dist[0] > 1800)  dist[0] = 60;

    // if(fabs(cam.ball.degree) < 60&&cam.ball.distance > 130&&!ballgo) {
    //     ballarea.start();
    //     ballgo = true;
    // }
    // if(!(fabs(cam.ball.degree) < 60&&cam.ball.distance > 130)) {
    //     ballarea.stop();
    //     ballarea.reset();
    //     ballgo = false;
    // }
    // if(true) {
    //     // ballgo = true;
    //     ballarea.stop();
    //     balldrive = true;
    // }
    // if(dist[0] > 300&&balldrive) {
    //     ballPush.start();
    //     ballarea.reset();
    //     balldrive = false;
    // }
    // if(ballPush.read() > 1.0) {
    //    ballPush.stop();
    //    ballPush.reset();
    //    ballgo = false;
    // }
    if(fabs(cam.ball.degree) < 65&&cam.ball.distance > 135&&!ballgo) {
        // ballarea.start();
        balldrive = true;
        ballgo = true;
    }
    // if(true) {
    //     // ballgo = true;
    //     ballarea.stop();
    //     balldrive = true;
    // }
    if(balldrive  && cam.goalD[0].isViewed) {
      if(goalDegree[1] > 20 && goalDegree[2] < 20) {
        if(dist[0] > 315) {
          ballPush.start();
          balldrive = false;
          goback = true;
          goback2 = false;
        }
      } else if(fabs(goalDegree[1]) <= 20){
        if(cam.goalD[1].distance < 134) {
          ballPush.start();
          balldrive = false;
          goback = false;
          goback2 = true;
        }
      } else if(fabs(goalDegree[2]) >= 20) {
        if(cam.goalD[2].distance < 134) {
          ballPush.start();
          balldrive = false;
          goback = false;
          goback2 = true;
        }
      } else {
        if(dist[0] > 450) {
          ballPush.start();
          balldrive = false;
          goback = true;
          goback2 = false;
        }
      }
    } else {
      if(dist[0] > 310) {
        ballPush.start();
        balldrive = false;
        goback = true;
        goback2 = false;
      }
    }
    // if(dist[0] > 330&&balldrive) {
    //     // ballPush.reset();
    //     ballPush.start();
    //     balldrive = false;
    // }
    if(ballPush.read() > 0.8) goback = false;
    if(ballPush.read() > 1.1) goback2 = false;
    if(ballPush.read() > 1.7) {
       ballPush.stop();
       ballPush.reset();
       ballgo = false;
    }
    // ui.led[0]->write(line.isOnBehindLine());
    // ui.led[0]->write(ballgo);
    // ui.led[1]->write(balldrive);
    if(cam.goalD[0].isViewed > 0) {
        goalDdata[0] = (GOALD_X - cos(cam.goalD[0].radian+imu.getCurrentRadian())*cam.goalD[0].rawDistance);
        goalDdata[1] = (GOALD_X - cos(cam.goalD[1].radian+imu.getCurrentRadian())*cam.goalD[1].rawDistance);
        goalDdata[2] = (GOALD_X - cos(cam.goalD[2].radian+imu.getCurrentRadian())*cam.goalD[2].rawDistance);
        // camY[1] = (0.0 - sin(cam.goalD[0].radian+imu.getCurrentRadian())*cam.goalD[0].rawDistance);
        // if(cam.goalD[0].rawDistance > GOALA_X) {
        //     weightX[3] = weightX[3] - (cam.goalD[0].rawDistance - GOALA_X) / GOALA_X;
        //     weightY[3] = weightY[3] - (cam.goalD[0].rawDistance - GOALA_X) / GOALA_X;
        // }
    } else {
        // weightX[3] = 0;
        // weightY[3] = 0;
    }

    if(t.read() > 1/10.0) {
        // stopBall();
        selfLocalization();
        debugSerial.printf("%f\r\n", sonic.getDistance(BACK));
        // debugSerial.printf("%d, %d, %d\r\n", cam.goalD[0].distance, cam.goalD[1].distance, cam.goalD[2].distance);
        // debugSerial.printf("%f, %d\r\n",fabs(cam.ball.degree),cam.ball.distance);
        // debugSerial.printf("%f, %f ,%f\r\n", goalDdata[0],goalDdata[1],goalDdata[2]);
        // debugSerial.printf("%d, %d, %d\r\n", cam.goalD[0].degree, cam.goalD[1].degree, cam.goalD[2].degree);
        // debugSerial.printf("%d, %d, %d\r\n", goalDegree[0], goalDegree[1], goalDegree[2]);
        // debugSerial.printf("%f, %d, %f\r\n",(cam.goalD[0].degree+imu.getCurrentDegree()),cam.goalD[0].degree,imu.getCurrentDegree());
        // debugSerial.printf("%d, %d\r\n", goalDegree[1], cam.goalD[1].degree);
        // debugSerial.printf("%d,%d,%d\r\n", FW.x, FW.y, FW.status);
        // debugSerial.printf("%d\r\n",goalDegree);
        // debugSerial.printf("%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n", botX[0], botY[0], ballAbsX[0], ballAbsY[0], sonicX[0], sonicY[0], sonicX[1], sonicY[1], camX[0], camY[0], camX[1], camY[1]);
        t.reset();
    }
    // debugSerial.printf("%f\r\n", sonic.getDistance(BACK));
    /*if(ui.isPushed(0)) {
        ui.buzzer = 0.5;
        imu.resetOffset();
        Thread::wait(50);
        ui.buzzer = 0.0;
    ui.led[0]->write(xLeft);
    ui.led[1]->write(xRight);
    if(cam.ball.isViewed > 0) {
        findBall = true;
    } else {
        findBall = false;
        ballStop = false;
    }
    if(ballStopFlag) {
      if(stopCnt > 3) {
        ballStopFlag = false;
        ballStop = true;
        stopCnt = 0;
      }
    } else {
      // stopCnt = 0;
    }
    if(dist[0] < 120 && cam.goalD.y < 50 && !ballStopFlag) {
      ballStopFlag = true;
    }
    if(stopCnt) ui.buzzer = 0.7;
    else ui.buzzer = 0.0;
    // if(dist[0] > 600 || ballYVector > 5) {
    dist[2] = sonic.getDistance(2);
    dist[1] = sonic.getDistance(1);
    dist[0] = sonic.getDistance(0);
    if(dist[0] < 100) dist[0] = 100;
    if(dist[0] > 900)  dist[0] = 110;
    if(dist[1] > 2000)  dist[1] = 0;
    // dist[1] = sonic.getDistance(1);
    // dist[2] = sonic.getDistance(2);
    // if(dist[2] > 2000)  dist[2] = 0;
    if(ballYVector > 3 || dist[0] > 320 || cam.goalD.y > 63) {
    // if(dist[0] > 320) {
      ballStop = false;
    }*/
}

void DefenseCat::selfLocalization()
{
    // // xSum = sonic.getDistance(FRONT) + sonic.getDistance(BACK);
    // xSum = sonic.getDistance(BACK);
    // ySum = sonic.getDistance(LEFT) + sonic.getDistance(RIGHT);
    //
    // for(int i = 0; i < 4; i++) {
    //     weightX[i] = 1.0;
    //     weightY[i] = 1.0;
    // }
    //
    // sonicX[0] = (-2130/2.0 + sonic.getDistance(BACK) * 1.0014 + 65.955);
    // sonicX[1] = (-2130/2.0 + sonic.getDistance(BACK) * 1.0014 + 65.955);
    // sonicY[0] = (1820/2.0 - sonic.getDistance(LEFT) * 1.0014 + 65.955);
    // sonicY[1] = (-1820/2.0 + sonic.getDistance(RIGHT) * 1.0014 + 65.955);
    //
    // if(fabs(imu.getCurrentDegree()) > 10.0) {
    //     weightX[1] = 0;
    //     weightX[0] = 0;
    //     weightY[0] = 0;
    //     weightY[1] = 0;
    // } else {
    //     weightX[0] = 0.5;
    //     weightX[1] = 0.5;
    //     weightY[0] = 0.5;
    //     weightY[1] = 0.5;
    //     if(xSum < 600) {
    //       weightX[1] = 0;
    //       weightX[0] = 0;
    //     }
    //     if(ySum < 1400) {
    //       weightY[0] = 0;
    //       weightY[1] = 0;
    //     }
    // }
    //
    // // if(cam.goalA.isViewed > 0) {
    // //     camX[0] = (GOALA_X + cos(cam.goalA.radian+imu.getCurrentRadian())*cam.goalA.rawDistance);
    // //     camY[0] = (0.0 - sin(cam.goalA.radian+imu.getCurrentRadian())*cam.goalA.rawDistance);
    // //     if(cam.goalA.rawDistance > GOALA_X) {
    // //         weightX[2] = weightX[2] - (cam.goalA.rawDistance - GOALA_X) / GOALA_X;
    // //         weightY[2] = weightY[2] - (cam.goalA.rawDistance - GOALA_X) / GOALA_X;
    // //     }
    // // } else {
    // //     weightX[2] = 0;
    // //     weightY[2] = 0;
    // // }
    // weightX[2] = 0;
    // weightY[2] = 0;
    // if(cam.goalD[0].isViewed > 0) {
    //     camX[1] = (GOALD_X - cos(cam.goalD[0].radian+imu.getCurrentRadian())*cam.goalD[0].rawDistance);
    //     camY[1] = (0.0 - sin(cam.goalD[0].radian+imu.getCurrentRadian())*cam.goalD[0].rawDistance);
    //     if(cam.goalD[0].rawDistance > GOALA_X) {
    //         weightX[3] = weightX[3] - (cam.goalD[0].rawDistance - GOALA_X) / GOALA_X;
    //         weightY[3] = weightY[3] - (cam.goalD[0].rawDistance - GOALA_X) / GOALA_X;
    //     }
    // } else {
    //     weightX[3] = 0;
    //     weightY[3] = 0;
    // }
    //
    // weightSum[0] = weightX[0] + weightX[1] + weightX[2] + weightX[3];
    // weightSum[1] = weightY[0] + weightY[1] + weightY[2] + weightY[3];
    // for(int i = 0; i < 4; i++) {
    //     weightX[i] = weightX[i] / weightSum[0];
    //     weightY[i] = weightY[i] / weightSum[1];
    // }
    //
    // botX[2] = sonicX[0]*weightX[0] + sonicX[1]*weightX[1] + camX[0]*weightX[2] + camX[1]*weightX[3];
    // botY[2] = sonicY[0]*weightY[0] + sonicY[1]*weightY[1] + camY[0]*weightY[2] + camY[1]*weightY[3];
    //
    // botX[0] = LOCAL_FIL * botX[1] + (1.0 - LOCAL_FIL) * botX[2];
    // botX[1] = botX[0];
    // botY[0] = LOCAL_FIL * botY[1] + (1.0 - LOCAL_FIL) * botY[2];
    // botY[1] = botY[0];
    //
    // ballAbsX[2] = botX[0] + cos(cam.ball.radian+imu.getCurrentRadian())*cam.ball.rawDistance;
    // ballAbsY[2] = botY[0] + sin(cam.ball.radian+imu.getCurrentRadian())*cam.ball.rawDistance;
    //
    // ballAbsX[0] = LOCAL_FIL * ballAbsX[1] + (1.0 - LOCAL_FIL) * ballAbsX[2];
    // ballAbsX[1] = ballAbsX[0];
    // ballAbsY[0] = LOCAL_FIL * ballAbsY[1] + (1.0 - LOCAL_FIL) * ballAbsY[2];
    // ballAbsY[1] = ballAbsY[0];

    localData[0] = 1*(!ui.toggle);
    localData[1] = 0;
    localData[2] = 10;
    localData[3] = 0;
    localData[4] = 11;

    FW.sendData(localData);
}

void DefenseCat::track(int mode)
{
    if(ui.toggle) {
      omni.driveCircular(0, 0, 0);
      return;
    }
    driveY = _driveY;
    // driveY = 0.0;
    // driveX = (100 - sonic.getDistance(BACK))/100;
    if(cam.goalD[0].isViewed){
    if(goalDegree[1] > 15 && goalDegree[2] < 15) {
      driveX = (110-dist[0])/30.0;
    // } else if(goalDegree[1] <= 7){
    //   driveX = (cam.goalD[1].distance-155)/10.0;
    // } else if(fabs(goalDegree[2]) <= 7) {
    //   driveX = (cam.goalD[2].distance-155)/10.0;
  } else if(fabs(goalDegree[2]) <= 15 || fabs(goalDegree[1]) <= 15){
      driveX = 0;
    }else {
      driveX = (70-dist[0])/30.0;
    }
  } else {
    driveX = (110-dist[0])/30.0;
  }

    // if()
    if(balldrive)  driveX = 0.8;
    if(goback || goback2) driveX = -0.8;
    if(cam.goalD[0].isViewed <= 0)  driveY = -(sonic.getDistance(LEFT)-sonic.getDistance(RIGHT))/10.0;
    if(driveX > 1.0)  driveX = 1.0;
    else if(driveX < -1.0)  driveX = -1.0;
    // if(driveY > 1.0)  driveY = 1.0;
    // else if(driveY < -1.0)  driveY = -1.0;
    // driveX = 0.0;
    lineProcess();
    omni.driveXY(driveX*(1-fabs(imu.getCalculationResult())), driveY*(1-fabs(imu.getCalculationResult())), imu.getCalculationResult());

    // if(ballStop) {
    //   driveY = 1.0;
    // }
}

void DefenseCat::lineProcess()
{
    if(line.isOnLeftLine() && !lineSonic[RIGHT]) {
        lineSonic[LEFT] = true;
    }
    if(line.isOnRightLine() && !lineSonic[LEFT]) {
        lineSonic[RIGHT] = true;
    }
    if(line.isOnBehindLine()) {
        lineSonic[BACK] = true;
    }
    if(lineSonic[BACK]) {
      driveX = 1.0;
    }
    if(lineSonic[LEFT]) {
        driveY = -1.0;
    }
    if(lineSonic[RIGHT]) {
        driveY = 1.0;
    }
    if(sonic.getDistance(BACK) > 320) {
      lineSonic[BACK] = false;
    }
    if(sonic.getDistance(LEFT) > 400) {
      lineSonic[LEFT] = false;
    }
    if(sonic.getDistance(RIGHT) > 400) {
      lineSonic[RIGHT] = false;
    }
}

void DefenseCat::stopBall()
{
  if(cam.ball.isViewed && !ui.toggle){
    currentBallDegree = cam.ball.degree;
    currentBallDistance = cam.ball.distance;
    if(fabs(currentBallDegree - beforeBallDegree) < 6
        && fabs(currentBallDistance - beforeBallDistance) < 6
        && fabs(cam.ball.degree) < 50
        && cam.ball.distance < 57 )
        // && !movedLeft
        // && !movedRight)
      stopCnt++;
    // else stopCnt--;
    if(stopCnt < 0) stopCnt = 0;
    beforeBallDegree = currentBallDegree;
    beforeBallDistance = currentBallDistance;
  } else {
    stopCnt = 0;
  }
}

void DefenseCat::stop()
{
    if(ui.toggle) {
          omni.driveCircular(0, 0, 0);
        return;
    }
    omni.driveCircular(0, 0, 0.0, 0.0,  imu.getCalculationResult());
}
void DefenseCat::goBack()
{
    if(ui.toggle) {
        omni.driveCircular(0, 0, 0);
        return;
    }
    driveY = _driveY;
    if(cam.goalD[0].isViewed){
      driveX = (110-dist[0])/30.0;
    } else {
    if(goalDegree[1] > 0 && goalDegree[2] < 0) {
      driveX = (110-dist[0])/30.0;
    } else {
      driveX = 0;
    }
  }
    if(!(cam.goalD[0].isViewed>0)) {
        driveY = -(sonic.getDistance(LEFT)-sonic.getDistance(RIGHT))/10.0;
    }
    if(driveX > 1.0)  driveX = 1.0;
    else if(driveX < -1.0)  driveX = -1.0;
    if(driveY > 1.0)  driveY = 1.0;
    else if(driveY < -1.0)  driveY = -1.0;
    lineProcess();
    omni.driveXY(driveX*(1-fabs(imu.getCalculationResult())), driveY*(1-fabs(imu.getCalculationResult())), imu.getCalculationResult());
}

void DefenseCat::xPidProcess()
{
  if (cam.goalD[0].degree > 0) goalDegree[0] = cam.goalD[0].degree-180;
  else goalDegree[0] = cam.goalD[0].degree+180;
  if (cam.goalD[1].degree > 0) goalDegree[1] = cam.goalD[1].degree-180;
  else goalDegree[1] = cam.goalD[1].degree+180;
  if (cam.goalD[2].degree > 0) goalDegree[2] = cam.goalD[2].degree-180;
  else goalDegree[2] = cam.goalD[2].degree+180;
  if(goalDegree[1] < 0) {
    movedRight = true;
  }
  if(goalDegree[2] > 0) {
    movedLeft = true;
  }
  if(movedRight && cam.ball.degree > 0.0 ) {
    movedRight = false;
  }
  if(movedLeft && cam.ball.degree < 0.0 ) {
    movedLeft = false;
  }
  if (cam.ball.isViewed) {
      if (movedRight) {
        // xPid.setSetPoint(60);
        xPid.setProcessValue(goalDegree[1]);
        _driveY =  xPid.compute();
        return;
      }
      if (movedLeft) {
        // xPid.setSetPoint(-40);
        xPid.setProcessValue(goalDegree[2]);
        _driveY =  xPid.compute();
        return;
      }
      if((fabs(cam.ball.degree) < 70 || balldrive) && cam.ball.distance > 128) {
        // xPid.setSetPoint(0);
          xPid.setProcessValue(cam.ball.degree);
          _driveY =  -xPid.compute();
          return;
      } else if(cam.ball.degree < 140 && cam.ball.degree > 0&& cam.ball.distance > 128) {
        // xPid.setSetPoint(0);
          xPid.setProcessValue(goalDegree[2]);
          _driveY = xPid.compute();
          return;
      } else if(cam.ball.degree > -140 && cam.ball.degree < 0&& cam.ball.distance > 128) {
        // xPid.setSetPoint(0);
          xPid.setProcessValue(goalDegree[1]);
          _driveY = xPid.compute();
          return;
      } else {
        // xPid.setSetPoint(0);
          xPid.setProcessValue(goalDegree[0]);
          _driveY = xPid.compute();
          return;
      }
  } else {
    xPid.setSetPoint(0);
      xPid.setProcessValue(goalDegree[0]);
      _driveY = xPid.compute();
      return;
  }
}

void DefenseCat::xPidLoop()
{
    while(true) {
       xPidProcess();
       Thread::wait(10);
    }
}

void DefenseCat::dribblerProcess()
{
    if(cam.ball.isViewed) {
        if(ui.toggle) {
            if(ui.isPushed(2))  kickUnit.dribble(DRIBBLE_SPEED);
            else  kickUnit.dribble(0.0);
            return;
        }
        if(fabs(cam.ball.degree) < 40)  kickUnit.dribble(DRIBBLE_SPEED);
        else  kickUnit.dribble(0.0);
        return;
    }
    if(ui.isPushed(2))  kickUnit.dribble(DRIBBLE_SPEED);
    else  kickUnit.dribble(0.0);
}

void DefenseCat::dribblerLoop()
{
    while(true) {
        dribblerProcess();
    }
}

void DefenseCat::calibration()
{

}

void DefenseCat::testDrive()
{
    if(ui.toggle) {
        testflag = false;
        stop();
        return;
    } else {
      if(ui.isPushed(1)&&!testflag) {
        testflag = true;
        test.reset();
        test.start();
      }
      if(!testflag) {
        stop();
        return;
      }
      if(test.read() > 1.0) {
        test.stop();
        stop();
        return;
      }
      omni.driveCircular(1.0-fabs(imu.getCalculationResult()),75*PI/180.0, imu.getCalculationResult());
      // omni.driveCircular(1.0, 60*PI/180.0, 0);
    }

}
