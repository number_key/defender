#include "ultra_sonic.h"

UltraSonic::UltraSonic()
{
    us015[0] = new US015int(us015_BACK_trig, us015_BACK_echo);
    us015[1] = new US015int(us015_LEFT_trig, us015_LEFT_echo);
    us015[2] = new US015int(us015_RIGHT_trig, us015_RIGHT_echo);

    us015[0]->setFinishCallback(callback(this, &UltraSonic::getDistanceBack));
    us015[1]->setFinishCallback(callback(this, &UltraSonic::getDistanceLeft));
    us015[2]->setFinishCallback(callback(this, &UltraSonic::getDistanceRight));
}

void UltraSonic::getDistanceBack(int dist)
{
    if(dist > 3 && dist < 20000) {
        distance[0] = LPF_COEFFICIENT * buffer[0] + (1.0 - LPF_COEFFICIENT) * dist;
        buffer[0] = distance[0];
    }
}
void UltraSonic::getDistanceLeft(int dist)
{
    if(dist > 3 && dist < 20000) {
        distance[1] = LPF_COEFFICIENT * buffer[1] + (1.0 - LPF_COEFFICIENT) * dist;
        buffer[1] = distance[1];
    }
}
void UltraSonic::getDistanceRight(int dist)
{
    if(dist > 3 && dist < 20000) {
        distance[2] = LPF_COEFFICIENT * buffer[2] + (1.0 - LPF_COEFFICIENT) * dist;
        buffer[2] = distance[2];
    }
}

float UltraSonic::getDistance(int number)
{
    return distance[number];
}
