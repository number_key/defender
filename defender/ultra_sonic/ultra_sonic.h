#ifndef ULTRA_SONIC_H
#define ULTRA_SONIC_H

#include "mbed.h"
#include "pin_config.h"
#include "US015_interrupt.h"

#define LPF_COEFFICIENT 0.0

class UltraSonic {
    public :
        UltraSonic();
        float getDistance(int number);

    private :
        US015int *us015[3];
        float buffer[3];
        float distance[3];

        void getDistanceBack(int dist);
        void getDistanceLeft(int dist);
        void getDistanceRight(int dist);
};

#endif
