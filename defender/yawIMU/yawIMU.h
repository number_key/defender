#ifndef YAW_IMU_H
#define YAW_IMU_H

#include "mbed.h"
#include "JY901.h"
#include "PID.h"

#define PI (3.14159265f)
#define KP 4.5
#define KI 70
#define KD 0.00017
#define T_SAMPLE 0.010
#define EXCESS_THRESHOLD 270.0
#define RESET_THRESHOLD 30.0

class YawIMU : public JY901 {
    public :
        YawIMU(PinName jyTX, PinName jyRX);
        void confirm();
        double getCalculationResult();
        double getCurrentDegree();
        double getCurrentRadian();
        double getRawDegree();
        void setPoint(double point);
        void resetOffset();
        void setGain(float kp, float ki, float kd);
        void reseetGain();
    private :
        PID yawPID;
        Thread thread;
        void threadLoop();
        double offsetDegree;
        int turnOverNumber;
        double beforeDegree;
    protected :
        double rawDegree;
        double currentDegree;
        double calculationResult;
};
#endif
