#include "yawIMU.h"

YawIMU::YawIMU(PinName jyTX, PinName jyRX) :
    JY901(jyTX, jyRX),
    yawPID(KP, KI, KD, T_SAMPLE)
{
    yawPID.setInputLimits(-360.0, 360.0);
    yawPID.setOutputLimits(-1.0, 1.0);
    yawPID.setSetPoint(0);
    yawPID.setBias(0);
    yawPID.setMode(AUTO_MODE);
    rawDegree = getYaw();
    beforeDegree = rawDegree;
    offsetDegree = rawDegree;
    thread.start(callback(this, &YawIMU::threadLoop));
}

void YawIMU::confirm()
{
    JY901::confirm();
    rawDegree = getYaw();
    if(rawDegree - beforeDegree < -EXCESS_THRESHOLD) ++turnOverNumber;
    if(rawDegree - beforeDegree > EXCESS_THRESHOLD) --turnOverNumber;
    // if(fabs(rawDegree - beforeDegree) > RESET_THRESHOLD  && fabs(rawDegree - beforeDegree) < EXCESS_THRESHOLD) resetOffset();
    if(turnOverNumber > 1) turnOverNumber = 1;
    if(turnOverNumber < -1) turnOverNumber = -1;
    currentDegree = rawDegree - offsetDegree + turnOverNumber * 360.0;
    beforeDegree = getYaw();
    yawPID.setProcessValue(currentDegree);
    calculationResult = yawPID.compute();
}

double YawIMU::getCalculationResult()
{
    return calculationResult;
}

double YawIMU::getCurrentDegree()
{
    return currentDegree;
}
double YawIMU::getCurrentRadian()
{
    return currentDegree * (PI/180.0);
}

double YawIMU::getRawDegree()
{
    return rawDegree;
}

void YawIMU::setPoint(double point)
{
    yawPID.setSetPoint(point);
}

void YawIMU::resetOffset()
{
    beforeDegree = getYaw();
    offsetDegree = getYaw();
    turnOverNumber = 0;
}

void YawIMU::setGain(float kp, float ki, float kd)
{
    yawPID.setTunings(kp, ki, kd);
}

void YawIMU::reseetGain()
{
    yawPID.setTunings(KP, KI, KD);
}

void YawIMU::threadLoop()
{
    while(true) {
        Thread::wait(10);
        confirm();
    }
}
