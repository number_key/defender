#ifndef Defense_CAT_H
#define Defense_CAT_H

#include "mbed.h"
#include "pin_config.h"
#include "fast_atan.h"
#include "UI.h"
#include "omni_wheel.h"
#include "yawIMU.h"
#include "OpenMV.h"
#include "line_sensors.h"
#include "kick_unit.h"
#include "ultra_sonic.h"
#include "XBee.h"

#define FIELD_X 2430
#define FIELD_Y 1820
#define GOALD_X (-915.0)
#define GOALA_X 915.0
#define LOCAL_FIL 0.990

#define BACK 0
#define LEFT 1
#define RIGHT 2

#define DRIBBLE_SPEED 0.25
#define SPEED 1.0
#define DIST_MIN 20
#define DIST_MAX 90
// #define BALL_KP 1.7
// #define BALL_KI 0.2
// #define BALL_KD 0.00013
#define BALL_KP 2.1
#define BALL_KI 0.6
#define BALL_KD 0.00035
#define GOAL_KP 1.2
#define GOAL_KI 0
#define GOAL_KD 0.00006
#define GOAL_CENTER_KP 1.4
#define GOAL_CENTER_KI 80
#define GOAL_CENTER_KD 0.000006
#define T_SAMPLE 0.010
class DefenseCat {
    public:
        DefenseCat();
        void updateSensors();
        void selfLocalization();
        void track(int mode);
        void stop();
        void goBack();
        void calibration();
        void testDrive();
        bool findBall;
    private:
        Serial debugSerial;
        OmniWheel omni;
        YawIMU imu;
        OpenMV cam;
        LineSensors line;
        KickUnit kickUnit;
        UI ui;
        XBee FW;
        Timer t;
        PID xPid;
        UltraSonic sonic;
        Thread threadxPid;
        Thread threadDribbler;
        Timer behindTimer;
        Timer ballPush;
        Timer ballarea;

        float driveX;
        float driveY;
        bool movedLeft;
        bool movedRight;
        // double goalDegree[3];
        int currentGoalDegree[3];
        int beforeGoalDegree[3];
        int dist[3];
        int beforeBallDegree;
        int currentBallDegree;
        int beforeBallDistance;
        int currentBallDistance;
        int stopCnt;
        double _driveY;
        bool ballStop;
        int ballX;
        int _beforeBallDegree;
        int _currentBallDegree;
        int ballDgreeVector;
        int beforeBallY;
        int currentBallY;
        int ballYVector;
        int beforeBallX;
        int currentBallX;
        int ballXVector;
        int beforeBackDistance;
        int currentBackDistance;
        int backDistance;
        bool removed;
        bool stopY;
        bool pulse;
        bool behindFlag;
        bool ballPidFlag;
        bool goalPidFlag;
        bool goalCenterFlag;
        bool sonicRangeChange;
        // bool xRight;
        // bool xLeft;
        bool lineSonic[3];
        bool cancel;
        bool ballStopFlag;

        bool ballgo;
        bool balldrive;
        bool goback;
        bool goback2;

        int goalDegree[3];

        float xSum;
        float ySum;
        float botX[3];
        float botY[3];
        float ballAbsX[3];
        float ballAbsY[3];
        float sonicX[2];
        float sonicY[2];
        float camX[3];
        float camY[3];
        float weightX[5];
        float weightY[5];
        float weightSum[2];

        float goalDdata[3];

        char localData[5];

        Timer test;
        bool testflag;

        void stopBall();
        void lineProcess();
        void calculateGoalDegree();
        void xPidProcess();
        void xPidLoop();
        void dribblerProcess();
        void dribblerLoop();
        void ballVectorLoop();
        void filterLoop();

};
#endif
