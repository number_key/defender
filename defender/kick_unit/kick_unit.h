#ifndef KICK_UNIT_H
#define KICK_UNIT_H

#include "mbed.h"
#include "pin_config.h"

class KickUnit {
    public:
        KickUnit();

        void kick();
        void dribble(float speed);
        DigitalIn isHeld;
    private:
        PwmOut dribbler;
        DigitalOut kicker;
        Timeout timeout;

        void kickEnd();
};
#endif
