#include "kick_unit.h"

KickUnit::KickUnit() :
    isHeld(HOLDsenser),
    dribbler(ESC_PWM),
    kicker(solenoid)
{
    dribbler.period(0.020);//20Hz
    dribbler.pulsewidth(0.010);

    kicker = true;
    kicker = false;
}

void KickUnit::kick() {
    kicker = true;
    timeout.attach(callback(this, &KickUnit::kickEnd), 0.1);
}

void KickUnit::dribble(float speed)
{
    dribbler.pulsewidth(0.010 + speed * 0.010);
}

void KickUnit::kickEnd()
{
    kicker = false;
}
