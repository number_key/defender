#include "fast_atan.h"

extern float fastAtan(float x)
{
	int i;
	float index;
	int index_int;
	float index_dec;


	if (x<0) return -fastAtan(-x);
	if (x == 0.0)   return 0.0;
	for (i = 1; i<=BLOCK_NUM_ATAN; i++)
	{
		if (x <= atan_table_delimit_val[i])
		{
			index = DEBIDE_ONE_BLOCK_ATAN * (i - 1) + ((x - atan_table_delimit_val[i - 1]) / atan_table_width[i - 1]);
			index_int = (int)index;
			index_dec = index - (float)index_int;
			return atan_table[index_int] + index_dec * ((atan_table[index_int + 1] - atan_table[index_int]));
		}
	}
	return atan_table[TABLE_SIXE_ATAN-1];

}

extern float fastAtan2(float y, float x)
{
	if (x >  0.0) return fastAtan(y / x);
	if (y >= 0.0 &&  x <  0.0) return fastAtan(y / x) + PI;
	if (y <  0.0 &&  x <  0.0) return fastAtan(y / x) - PI;
	if (y >  0.0 &&  x == 0.0) return PI / 2.0;
	if (y <  0.0 &&  x == 0.0) return -PI / 2.0;
	if (y == 0.0 &&  x == 0.0) return 0.0;  //普通は定義されない.
	return 0.0;
}
