#include "UI.h"

UI::UI():
    // debugSerial(USBTX, USBRX, 230400),
    buzzer(BUZZER),
    toggle(TOGGLE_SWITCH)
{
    buzzer.period(1.0 / 380.0);
    buzzer = 0.0;
    led[0] = new DigitalOut(LED_OUT0);
    led[1] = new DigitalOut(LED_OUT1);
    // led[2] = new DigitalOut(LED_OUT2);

    button[0] = new DigitalIn(UI_SWITCH0, PullDown);
    button[1] = new DigitalIn(UI_SWITCH1, PullDown);
    button[2] = new DigitalIn(UI_SWITCH2, PullDown);

    debugData = new char[4];

    //tick.attach(callback(this, &UI::callbackUI), UI_FREQ);
}

void UI::callbackUI()
{
    /*
    debugSerial.putc(0x55);
    debugSerial.putc(0xaa);
    debugSerial.putc(debugData[0]);
    debugSerial.putc(debugData[1]);
    debugSerial.putc(debugData[2]);
    debugSerial.putc(debugData[3]);
    */
}

bool UI::isPushed(int buttonNumber)
{
    return button[buttonNumber]->read();
}

void UI::lChika(int number)
{
    for(int i = 0; i < number; i++) {
        led[0]->write(true);
        Thread::wait(50);
        led[0]->write(false);
        led[1]->write(true);
        Thread::wait(50);
        led[1]->write(false);
        // led[2]->write(true);
        // Thread::wait(50);
        // led[2]->write(false);
    }
}

void UI::pee(float period, float time)
{
    buzzer.period(period);
    buzzer = 0.5;
    timeout.attach(callback(this, &UI::peeEnd), time);
}

void UI::peeEnd()
{
    buzzer = 0.0;
}
