#ifndef UI_H
#define UI_H

#include "mbed.h"
#include "pin_config.h"

#define UI_FREQ 0.5
class UI {
    public :
        UI();
        bool isPushed(int buttonNumber);
        void lChika(int number);
        void pee(float period, float time);
        // DigitalOut *led[3];
        DigitalOut *led[2];
        DigitalIn toggle;
        char *debugData;
        PwmOut buzzer;

    private :
        DigitalIn *button[3];
        // RawSerial debugSerial;
        Ticker tick;
        Timeout timeout;

        void callbackUI();
        void peeEnd();
};
#endif
