#include "line_sensors.h"

LineSensors::LineSensors()
{
    lineSensor[0] = new AnalogIn(LINE_INPUT2);
    lineSensor[1] = new AnalogIn(LINE_INPUT3);
    lineSensor[2] = new AnalogIn(LINE_INPUT4);
    lineSensor[3] = new AnalogIn(LINE_INPUT5);
    lineSensor[4] = new AnalogIn(LINE_INPUT6);
    lineSensor[5] = new AnalogIn(LINE_INPUT7);
    lineSensor[6] = new AnalogIn(LINE_INPUT8);

    //lineChecker.attach(callback(this, &LineSensors::checkLine), 0.0050);
    thread.start(callback(this, &LineSensors::threadLoop));
}

void LineSensors::checkLine()
{
    for(int i = 0; i < 7; i++) {
        if(lineSensor[i]->read() > LINE_THRESHOLD) {
            isOn[i] = true;
            onLine = 1500;
        } else {
            isOn[i] = false;
        }
    }
    onLine--;
    if(onLine < 0) onLine = 0;
    onFrontLine = isOn[6];
    onLeftLine = (isOn[0] || isOn[1]);
    onBehindLine = (isOn[2] || isOn[3]);
    onRightLine = (isOn[4] || isOn[5]);
}

bool LineSensors::isOnFrontLine()
{
    return onFrontLine;
}

bool LineSensors::isOnRightLine()
{
    return onRightLine;
}
bool LineSensors::isOnLeftLine()
{
    return onLeftLine;
}
bool LineSensors::isOnBehindLine()
{
    return onBehindLine;
}

int LineSensors::isOnLine()
{
    return (onLine);
}

float LineSensors::getDepth()
{
    return depth;
}

void LineSensors::threadLoop() {
    while(true) {
        checkLine();
    }
}
