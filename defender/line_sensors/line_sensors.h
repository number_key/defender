#ifndef LINE_SENSORS_H
#define LINE_SENSORS_H

#include "mbed.h"
#include "pin_config.h"
#include "US015_interrupt.h"

#define LINE_THRESHOLD 0.9

class LineSensors {
    public:
        LineSensors();
        void checkLine();
        bool isOnFrontLine();
        bool isOnRightLine();
        bool isOnLeftLine();
        bool isOnBehindLine();
        int isOnLine();
        float getDepth();
        bool isOn[7];

    private:
        AnalogIn *lineSensor[7];
        Thread thread;
        float depth;

        int onLine;
        bool onFrontLine;
        bool onRightLine;
        bool onLeftLine;
        bool onBehindLine;
        void threadLoop();
};

#endif
