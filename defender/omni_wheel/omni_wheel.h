#ifndef OMNI_WHEEL_H
#define OMNI_WHEEL_H

#include "mbed.h"
#include "fast_atan.h"
#include "omni_unit.h"
#include "pin_config.h"

#define LPF_K 0.95


class OmniWheel {
    public:
        OmniWheel();
        void driveCircular(double r, double theta, double gX, double gY, double moment);
        void driveCircular(double r, double theta, double moment);
        void driveXY(double X, double Y, double gX, double gY, double moment);
        void driveXY(double X, double Y, double moment);
        void overHead();
        void stop();

    private:
        OmniUnit omni;
        PwmOut *motorPwm[4];
        double speed[4][2];

        void drive();
};
#endif
