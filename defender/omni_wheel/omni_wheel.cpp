#include "omni_wheel.h"

OmniWheel::OmniWheel():
    omni(4)
{
    motorPwm[0] = new PwmOut(MD_PWM0);
    motorPwm[1] = new PwmOut(MD_PWM1);
    motorPwm[2] = new PwmOut(MD_PWM2);
    motorPwm[3] = new PwmOut(MD_PWM3);

    for(int i = 0; i < 4; i++) {
        motorPwm[i]->period(1.0 / 16980.0);
        motorPwm[i]->write(0.5);
    }
    motorPwm[0]->period(1.0 / 156.7981743926997);
    Thread::wait(100);
    motorPwm[0]->period(1.0 / 16980.0);
    motorPwm[1]->period(1.0 / 147.99776908465376);
    Thread::wait(100);
    motorPwm[1]->period(1.0 / 16980.0);
    motorPwm[2]->period(1.0 / 124.45079348883237);
    Thread::wait(100);
    motorPwm[2]->period(1.0 / 16980.0);
    motorPwm[3]->period(1.0 / 88.0);
    Thread::wait(100);
    motorPwm[3]->period(1.0 / 16980.0);
    motorPwm[0]->period(1.0 / 73.99888454232688);
    Thread::wait(100);
    motorPwm[0]->period(1.0 / 16980.0);
    motorPwm[1]->period(1.0 / 131.85102276514797);
    Thread::wait(100);
    motorPwm[1]->period(1.0 / 16980.0);
    motorPwm[2]->period(1.0 / 147.99776908465376);
    Thread::wait(100);
    motorPwm[2]->period(1.0 / 16980.0);
    motorPwm[3]->period(1.0 / 209.3004522404789);
    Thread::wait(100);
    for(int i = 0; i < 4; i++) {
        motorPwm[i]->period(1.0 / 16980.0);
        motorPwm[i]->write(0.5);
    }

    omni.wheel[0].setRadian(-1.0 * PI / 4.0);
    omni.wheel[1].setRadian(-3.0 * PI / 4.0);
    omni.wheel[2].setRadian(3.0 * PI / 4.0);
    omni.wheel[3].setRadian(1.0 * PI / 4.0);

    //omni.wheel[0].setDistance(1.0);
    //omni.wheel[1].setDistance(0.0);
    //omni.wheel[2].setDistance(0.0);
    //omni.wheel[3].setDistance(0.0);
}


void OmniWheel::driveCircular(double r, double theta, double gX, double gY, double moment)
{
    omni.computeCircular(r, theta, gX, gY, moment);
    drive();
}

void OmniWheel::driveCircular(double r ,double theta, double moment)
{
    omni.computeCircular(r, theta, 0, 0, moment);
    drive();
}

void OmniWheel::driveXY(double X, double Y, double gX, double gY, double moment)
{
    omni.computeXY(X, Y, gX, gY, moment);
    drive();
}

void OmniWheel::driveXY(double X, double Y, double moment)
{
    // double radian;
    // radian = atan2(fabs(X),fabs(Y));
    // if(radian > PI / 4.0) radian = PI / 4.0;
    // if(X == 0 && Y == 0) radian = PI / 4.0;

    // omni.wheel[0].setRadian(-radian);
    // omni.wheel[1].setRadian(-PI + radian);
    // omni.wheel[2].setRadian(PI - radian);
    // omni.wheel[3].setRadian(radian);

    omni.computeXY(X, Y, 0, 0, moment);
    drive();
}

void OmniWheel::overHead()
{
    driveCircular(0, 0, 0, 0, 0.1);
    wait(0.3);
    driveCircular(0, 0, 0, 0, 0.3);
    wait(0.32);
    driveCircular(0, 0, 0, 0, 1.0);
    wait(0.25);

}

void OmniWheel::stop()
{
    omni.computeCircular(0, 0, 0, 0, 0);
    drive();
}

void OmniWheel::drive()
{
    for(int i = 0; i < 4; i++) {
      speed[i][1] = 0.5 + omni.wheel[i].getOutput() / 2.0;
      if(speed[i][1] < 0.05) speed[i][1] = 0.05;
      if(speed[i][1] > 0.95) speed[i][1] = 0.95;
      speed[i][0] = speed[i][0] * LPF_K + speed[i][1] * (1.0 - LPF_K);
      motorPwm[i]->write(speed[i][0]);
    }
}
