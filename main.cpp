#include "mbed.h"
#include "pin_config.h"
#include "defender.h"

DefenseCat cat;
Timer t;

int main()
{
   int cnt = 0;
   bool stopflag = false;
   t.start();
   t.reset();
   while(true) {
       cat.updateSensors();
       // cat.testDrive();
       // cat.selfLocalization();
       // cat.stop();
       if(cat.findBall) {
           cat.track(2);
           // cnt = 0;
           t.stop();
           t.reset();
           stopflag = true;
       } else {
         // cat.goBack();
           // cat.stop();
           // cat.goBack();
           if(stopflag) t.start();
           if(t.read() < 0.5 )  cat.stop();
           else cat.goBack();
           stopflag = false;
           // cnt++;
       }
       // cat.goBack();
       // //pc.printf("%dus\r\n", t.read_us());
       // t.reset();
   }
}
